const Profile = require('../models/Profile');
const User = require('../models/User');

exports.getProfile = async(req,res)=>{
    try{
        const profile = await Profile.findOne({user : req.user.id}).populate('user','name');
        if(!profile){
            return res.status(400).json({msg : 'There is no profile for this user'});
        }
        res.json(profile);
    }
    catch(err){
        res.status(500).send("Error");
    }
};

exports.addProfile = async(req,res) => {
    const {name, userName, bio, mobileNumber, gender} = req.body;
    let userDetail = {};
    userDetail.user = req.user.id,
    userDetail.name = name,        
    userDetail.userName = userName,        
    userDetail.bio = bio,
    userDetail.mobileNumber = mobileNumber,       
    userDetail.gender = gender,
    userDetail.profileImage = req.file.path
    try{
        let profile = await Profile.findOne({user : req.user.id});

        if(profile){
            profile = await Profile.findOneAndUpdate(
                {user : req.user.id},
                {$set : userDetail},
                {new : true}
            );
            return res.json(profile);
        }
        profile = new Profile(userDetail);
        await profile.save();
        res.json(profile);
    }
    catch(err){
        res.status(500).send("Error");  
    }
};
exports.getAllProfile = async(req,res) => {
    try{
        const profiles = await Profile.find().populate('user','name');
        res.json(profiles);
    }
    catch(err){
        res.status(500).send('Error');
    }
};
exports.getProfileById = async(req,res) => {
    try{
        const profile = await Profile.findOne({ user : req.params.userId}).populate('user', 'name');
        if(!profile)
            return res.status(400).json({msg : "Profile not found"});
        res.json(profile);
    }
    catch(err){
        if(err.kind == "ObjectId")
            return res.status(400).json({msg : "Profile not found"});
        res.status(500).send('Error');
    }
};
exports.deleteProfile = async(req,res) => {
    try{
        await Profile.findOneAndRemove({ user : req.user.id});
        await User.findOneAndRemove({ _id : req.user.id});
        res.json({msg : "Deleted Successfully"});
    }
    catch(err){
        res.status(500).send('Error');
    }
};

exports.search = async(req, res)=>{
    try{
        const profile = await Profile.findOne({userName : req.body.userName}).populate('user',
        'name');

        if(!profile){
            return res.status(400).json({msg : 'No User Found'});
        }
        res.json(profile);
    }
    catch(err){
        res.status(500).send("Error");
    }
};

exports.follow = async(req,res) => {
    try{
        await User.findByIdAndUpdate(
            {_id : req.body.followId}, 
            {$push : {following : req.user.id}}, 
            {new : true},(err)=>{
                if(err) throw err;
                else{
                    User.findByIdAndUpdate(
                        {_id : req.user.id},
                        {$push : {following : req.body.followId}},
                        {new : true})
                        .then(response => {
                            res.json(response);
                        })
                        .catch(err => {
                            return res.status(400).json({error : err});
                        })
                }
            }
        );
    }
    catch(err){
        res.json({message : err});
    }
};

exports.unfollow = async(req,res) => {
    try{
        await User.findByIdAndUpdate(
            {_id : req.body.followId}, 
            {$pull : {following : req.user.id}}, 
            {new : true},(err)=>{
                if(err) throw err;
                else{
                    User.findByIdAndUpdate(
                        {_id : req.user.id},
                        {$pull : {following : req.body.followId}},
                        {new : true})
                        .then(response => {
                            res.json(response);
                        })
                        .catch(err => {
                            return res.status(400).json({error : err});
                        })
                }
            }
        );
    }
    catch(err){
        res.status(500).json({message : err});
    }
};