const Post = require('../models/Post');
const User = require('../models/User');

exports.addPost = async(req, res) => {
    try{
        const user = await User.findById(req.user.id).select('-password');    
        const newPost = new Post({
            post : req.file.path,
            caption : req.body.caption,
            user : req.user.id
        });
        console.log(newPost);
        const post = await newPost.save();
        res.json(post);
    }
    catch(err){
        res.status(500).send('Error');
    }
};

exports.getPost = async(req, res) => {
    try{
        const posts = await Post.find().sort({ date : -1});
        res.json(posts);
    }
    catch(err){
        res.status(500).send('Error');
    }
};

exports.feed = async(req, res) => {
    try{
        const posts = await Post.find({user : {$in : req.user.following}}).populate('user').sort('-date');
        res.json(posts);
    }
    catch(err){
        res.status(500).send('Error');
    }
};

exports.getPostById = async(req, res) => {
    try{
        const post = await Post.findById(req.params.postId).sort({ date : -1});
        if(!post){
            return res.status(404).json({ msg : 'Post not found'});
        }
        res.json(post);
    }
    catch(err){
        if(err.kind === 'ObjectId'){
            return res.status(404).json({ msg : 'Post not found'});
        }
        res.status(500).send('Error');
    }
};

exports.deletePostById = async(req, res) => {
    try{
        const post = await Post.findById(req.params.postId);
        if(!post){
            return res.status(404).json({ msg : 'Post not found'});
        }
        if(post.user.toString() !== req.user.id){
            return res.status(401).json({ msg : 'User not Authorized'});
        }
        await post.remove();
        res.json({ msg : 'Posed Deleted'});
    }
    catch(err){
        if(err.kind === 'ObjectId'){
            return res.status(404).json({ msg : 'Post not found'});
        }
        res.status(500).send('Error');
    }
};

exports.likePostById = async(req, res) => {
    try{
        const post = await Post.findById(req.params.id);
        
        if(post.likes.filter(like => like.user.toString() === req.user.id).length>0){
            return res.status(400).json({ msg : 'Post already liked'});
        }
        post.likes.unshift({ user : req.user.id});
        await post.save();

        res.json(post.likes);
    }
    catch(err){
        console.log(err);
        res.status(500).send('Server Error');
    }
};

exports.unLikePostById = async(req, res) => {
    try{
        const post = await Post.findById(req.params.id);
        
        if(post.likes.filter(like => like.user.toString() === req.user.id).length===0){
            return res.status(400).json({ msg : 'Post has not yet been liked'});
        }
        const removeIndex = post.likes.map(like => like.user.toString().indexOf(req.user.id));
        post.likes.splice(removeIndex, 1);
        await post.save();

        res.json(post.likes);
    }
    catch(err){
        res.status(500).send('Server Error');
    }
};

exports.commentPostById = async(req, res) => {
    try{
        const user = await User.findById(req.user.id).select('-password');
        if(user){
            const post = await Post.findByIdAndUpdate(req.params.postId, { $push: { comments: {user : req.user.id, comment : req.body.comment} }}, { new: true });
          
            post.comments.unshift(post);
            res.json(post.comments);
        }
    }
    catch(err){
        res.status(500).send('Error');
    }
};