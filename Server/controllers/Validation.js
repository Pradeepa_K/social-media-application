const {check, validationResult} = require('express-validator');

exports.Auth = (req, res, next) => {
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password is required').isLength({min : 6})
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array()});
    }
    next();
}
exports.Post = (req, res, next) => {
    check('post', 'Post is Empty').not().isEmpty()
    check('caption', 'Add caption for post').not().isEmpty()
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({ errors : errors.array});
    }
    next();
}
exports.Comment = (req, res, next) => {    
    check('comment', 'Add').not().isEmpty();
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({ errors : errors.array});
    }
    next();
}
exports.Profile = (req, res, next) => {    
    check('name', 'Name is required').not().isEmpty(),
    check('userName', 'User Name is required').not().isEmpty(),
    check('bio', 'Add your Bio').not().isEmpty(),
    check('mobileNumber', 'Mobile Number is required').not().isEmpty(),
    check('gender', 'Add your Gender').not().isEmpty(),
    check('profileImage', 'Add your profile pic').not().isEmpty();
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({ errors : errors.array});  
    }
    next();
}
exports.User = (req, res, next) => {    
    check('name', 'Name is required').not().isEmpty(),
    check('email', 'Enter a valid email').isEmail(),
    check('password', 'Enter a password with 6 or more characters').isLength({min : 6})
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array()});
    }
    next();
}