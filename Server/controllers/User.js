const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.addUser = async(req, res)=>{
        const {name, email, password } = req.body;
        let user = await User.findOne({email});
        try {
            if(user){
                res.status(400).json({errors : [{msg : 'User already exists'}]})
            }
            user = new User({
                name,
                email,
                password
            });
            const saltPassword = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(password, saltPassword);
            await user.save();
            const payload = {
                user : {
                    id : user.id
                }
            }
            
        jwt.sign(payload, process.env.jwtSecret,
        {expiresIn : 360000},
        (err, token) => {
            if(err) throw err;
            res.status(400).json(token);
        });
        }
        catch(err){
            res.json({message : err});
        }      
};

exports.getUser = async (req,res)=>{
    try{
        const Alluser = await User.find();
        res.json(Alluser);
    }
    catch(err){
        res.json({message : err});
    }
};

exports.getUserById = async (req,res)=>{
    try{
        const user = await User.findById({_id : req.params.id});
        res.json(user);
    }
    catch(err){
        res.json({message : err});
    }
};