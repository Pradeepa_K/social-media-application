const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const User = require('../controllers/User');
const validate = require('../controllers/Validation');

router.post('/addUser', validate.User, User.addUser);
router.get('/getUser', auth, User.getUser);
router.get('/getUser/:id', auth, User.getUserById);

module.exports = router;