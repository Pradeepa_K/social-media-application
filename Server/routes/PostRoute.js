const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const Post = require('../controllers/Post');
const multer = require('multer');
var upload = multer({ dest: 'uploads/' });
const validate = require('../controllers/Validation');

router.post('/addPost',upload.single('post'), validate.Post, auth, Post.addPost);
router.get('/getPost', auth, Post.getPost);
router.get('/feed', auth, Post.feed);
router.get('/getPost/:postId', auth, Post.getPostById);
router.delete('/deletePost/:postId', auth, Post.deletePostById);
router.put('/like/:id', auth, Post.likePostById);
router.put('/unLike/:id', auth, Post.unLikePostById);
router.post('/comment/:postId', auth, validate.Comment, Post.commentPostById);

module.exports = router;                    