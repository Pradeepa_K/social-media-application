const express = require('express');
const router = express.Router();
const Auth = require('../controllers/Auth');
const auth = require('../middleware/auth');
const validate = require('../controllers/Validation');

router.post('/login', validate.Auth, Auth.addAuth);
router.get('/getAuth', auth, Auth.getAuth);

module.exports = router;