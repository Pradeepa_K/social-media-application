const express = require('express');
const router = express.Router();
const auth = require("../middleware/auth");
const Profile = require('../controllers/Profile');
const multer = require('multer');
const validate = require('../controllers/Validation');

const storage = multer.diskStorage({
    destination : function(req, file, cb){
        cb(null, './uploads/');
    },
    filename : function(req, file, cb){
        cb(null, Date.now() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'|| file.mimetype === 'image/jpg'){
        cb(null, true);
    }
    else{
        cb(null, false);
    }
}

const upload = multer({
    storage : storage, 
    limits : {
        fileSize : 1024 * 1024 * 5
    },
    fileFilter : fileFilter
});

router.get('/getProfile', auth, Profile.getProfile);
router.post('/addProfile', upload.single('profileImage'), auth, validate.Profile, Profile.addProfile);
router.get('/getAllProfile',auth, Profile.getAllProfile);
router.get('/getProfile/:userId',auth, Profile.getProfileById);
router.delete('/deleteProfile', auth, Profile.deleteProfile);
router.get('/search', auth, Profile.search);
router.put('/follow', auth, Profile.follow);
router.put('/unfollow', auth, Profile.unfollow);

module.exports = router;