const mongoose = require('mongoose');
const postSchema = mongoose.Schema({
    user : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'user'
    },
    post : {
        type : String,
        required : true
    },
    caption : {
        type : String,
        required : true
    },   
    likes : [
        {
            user : { type : mongoose.Schema.Types.ObjectId, ref : 'user' }
        }
    ],
    comments : [
        {
            user : { type : mongoose.Schema.Types.ObjectId, ref : 'user'},
            comment : { type : String, required : true }
        }
    ],
    date : {
        type :Date,
        default : Date.now
    }
});
module.exports = mongoose.model('post', postSchema);