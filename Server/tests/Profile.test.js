const User = require('../models/User')
const Profile = require('../models/Profile');

describe('Unit Tests for Profile route', () => {
    beforeEach(() => {
      jest.setTimeout(500000);
    });

    test('Should create a new profile', async () => {
      try{
        const userDetail = {
          name : 'test',        
          userName : 'test_t',        
          bio : 'Tester',
          mobileNumber : '7894454121',       
          gender : 'Female',
          profileImage : 'Photo'
        }
        let profile = await Profile.findOne({user : id});
        if(profile){
          profile = await Profile.findOneAndUpdate(
              {user : id},
              {$set : userDetail},
              {new : true}
          );
        }
        profile = new Profile(userDetail);
        await profile.save();
      } 
      catch(err){
        expect(500);
      }
    });

    test('should display the profile detail', async() => {
        try{
            const profile = await Profile.findOne({ user : id});
            if(!profile)
                expect(200);
        }
        catch(err){
            expect(500);
        }             
    });

    test('Should and display the user profile', async() => {
      try{
          const profile = await Profile.findOne({userName : name});
          if(!profile)
            expect(400);
          else
            expect(200);
      }
      catch(err){
          expect(500);
      }             
    });

    test('Should Delete the profile and user', () => {
      try{
        Profile.deleteOne(profileId);
        User.deleteOne(userId);
        expect(204);
      }
      catch(err){
        expect(500);
      }    
    });
});