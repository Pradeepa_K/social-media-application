const Posts = require('../models/Post');
let postId;

test('Should create a new post', async () => {
  try{
    const user = User.findOne({ user : id });
    const post = new Posts({
      post: 'Test Photo',
      caption: 'Testing Post',
      user: user._id,
    });

    await post.save();
    postId = post._id;
  } 
  catch(err){
    expect(500);
  }
});

test('Should display the post of the given user', () => {
    const post = Posts.findById(postId)
      if(!post)
        expect(404);
      else
        expect(200);
});

test('Should Delete the post', () => {
  try{
    const post = Posts.deleteOne(postId)
      if(!post)
        expect(404);
      else
        expect(204);
  }
  catch(err){
    expect(500);
  }    
});