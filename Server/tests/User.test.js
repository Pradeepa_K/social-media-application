const User = require('../models/User');
const testData = require('../testData');
let userId;

test('Should register as new User', async() => {        
    try{
        const user = new User(testData);
        await user.save();

        userId = user._id;

        const checkUser = await User.findOne(userId);
        if(!checkUser){
            expect(200);
        }
        else{
            expect(409);
        }
    }
    catch(err){
        expect(500);
    }
});

test('Should display the user details', () => {
    const user = User.findById(userId)
      if(!user)
        expect(404);
      else
        expect(200);
});