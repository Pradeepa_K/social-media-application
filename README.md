# Social Media Application

## Objective:

Build RESTful APIs for a social media application.

## Features:

* User can sign up / log in.
* Users can fill up profile details, upload profile pics.
* Users will have a feed where users can see posts of friends and post their own posts.
* List all the friends.
* Search and send friend requests.
* Accept/decline pending friend requests.

---
### Tech stack
> Node, MongoDB.

Application is running on https://social-media-mern-application.herokuapp.com/

### Postman link

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/873d3829a7981f718fbc?action=collection%2Fimport)
